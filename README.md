# Ruby Standard Style

A gem that wraps Rubocop to provide a configuration that attempts to follow the [Standard] Ruby Style Guide as closely as possible.